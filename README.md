# Raspberry Pi Zero Datalogger

Using a Raspberry Pi Zero as a cheap and easy use for a datalogger. The idea is to log the temperature of a freezer, send it to NREC and send an alarm if the temperature rise above a threshold.

NREC (<www.nrec.no>) is a cloud service for research and education, provided by UiO and UiB. NREC is an IaaS, which stands for Infrastructure as a Service, and is a service where UiO users can provision virtual machines (servers) with network and storage. Resources, limited by a quota, are tied to "projects".

## Example of usage

- <https://en0.ch/2017/06/logging-temperature-and-humidity-with-a-raspberry-pi-zero-w/>
- <https://www.instructables.com/Raspberry-Pi-Zero-W-Datalogger/>
<<<<<<< HEAD
=======
- <https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/>

## Setting up NREC server

NREC (<www.nrec.no>) is a cloud service for research and education, provided by UiO and UiB. The idea is to use it as data storage and to post process the data (if needed). Following steps have to be done to set up the NREC server:

- Create a Linux virtual machine <https://uh-iaas.readthedocs.io/create-virtual-machine.html>
- Allow IPv4 and IPv6 for "SSH and ICMP": <https://docs.nrec.no/security-groups.html>
- If PC doesn't have an IPv6 address there is a way to use <login.uib.no> as an IPv4-to-IPv6 proxy: <https://docs.nrec.no/ssh.html#connecting-through-a-proxy>

The SNOWISO virtual machine created on NREC can be access:

``
ssh ubuntu@2001:700:2:8301::1379
``

## Important information

- Raspberry Pi 400: Communicate with Raspberry Pi Zero
  - Name: ipi-400
  - PW: snowiso
  - IP address: 192.168.0.146
- Raspberry Pi Zero: Log temperature
  - Name: ipi-zero
  - IP address: 192.168.0.161
  - PW: snowiso
>>>>>>> e2f9510 (Push existing project to GitLab)

## Requirement packages/softwares

- Python 3.9.12
- RSYNC

## List of hardware

What is need:

- Raspberry Pi Zero W
- Micro SD card
- USB cable or USB power supply
- Computer with a USB card reader
- Optional (but useful):
  - miniHDMI -> HDMI adapter (to connect the Pi to a screen)
  - USB OTG adapter (to connect a keyboard to the Pi)

## Authors and acknowledgement

- Pirmin Philipp Ebner: <pirmin.ebner@uib.no>

## License

This project is licensed under the terms of the MIT license.

## Project status/Roadmap

Improvement:

- Communicate with other Raspberry Pi zeros
