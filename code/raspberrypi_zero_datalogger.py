#!/usr/bin/env python3

import os
import glob
import time
import datetime
import pandas as pd
import smtplib
import ssl
import matplotlib.pyplot as plt
# https://realpython.com/python-send-email/#sending-fancy-emails
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

class Read_temperature():
    """ Class to read the temperature sensor and write out the time and
    actual temperature
    """
    def __init__(self, path_data='./data/', path_plot='./alarm_mails/'):
        # Load GPIO
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        # Path to the One-Wire interface
        self.base_dir = '/sys/bus/w1/devices/'
        self.device_folder = glob.glob(self.base_dir + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'
        self.path_data = path_data
        self.path_plot = path_plot


    def read_temp_raw(self):
        """ Function to read raw temperature data """
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines


    def read_temp(self):
        """ Function to get real temperature data """
        # Get raw data
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        # Get current time
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # Get real temperature
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = int(temp_string) / 1000.0
            return current_time, temp_c


    def log_sensor_data(self, file_name):
        """ Write the averaged 10 second logged temperature data to a file. 
        A new file will be writen at midnight"""
        file = os.path.join(self.path_data, file_name)
        with open(file, 'a') as f:
            # File path
            file_name = os.path.join(self.path_data, file_name)
            # Log data every 10 seconds and save to file every 1 minutes
            for _ in range(6):
                # Get sensor data (dummy data for demonstration)
                sensor_time, sensor_data = self.read_temp()
                # Write data with timestamp to the file
                f.write(f"{sensor_time}, {sensor_data}\n")
                print('Time {}, Temperature {}'.format(sensor_time, sensor_data)) 
                # Wait for 10 seconds
                time.sleep(10)


    def read_data(self, file, skiprows=0, start=False):
        """
        Read picolog data with an header

        Parameters
        ----------
        file : str
            Path to picolog file
        skiprows : int
            Skip rows
        start :datetime
            Add a start date to the time column
        
        Returns
        -------
        data : Panda table
            Panda table of the picolog data
        """

        # Read csv file
        data = pd.read_csv(file, sep=',',
                        skiprows=skiprows)
        data.columns.values[0] = 'Time'
        data.columns.values[1] = 'Temperature (ºC)'

        if start:
            time_ = data['Time'].str.split(':', expand=True).astype(float)
            data['Time'] = (start + time_[0]*datetime.timedelta(hours=1)
                                + time_[1]*datetime.timedelta(minutes=1)
                                + time_[2]*datetime.timedelta(seconds=1))
        # Set index
        data['Time'] = pd.to_datetime(data['Time'])
        data = data.set_index('Time')
        return data


    def get_figure(self):
        """ Create a figure of the last 5 hours"""
        # Get all sensor data files
        sensor_files = sorted([os.path.join(self.path_data, i) 
                                for i in os.listdir(self.path_data)
                                if i.endswith('.csv')])
        # Load the last two files
        sensor_files_load = sensor_files[-2:]
        # Load data
        pd_sensor = pd.concat([self.read_data(file)
                                        for file in sensor_files_load])
        # Plot data and save it
        time_plot = datetime.datetime.now().strftime('%Y%m%d%H%M')
        plot = pd_sensor.plot(title='Actual temperature at {}'.format(time_plot))
        # File to save
        save_plot = os.path.join(self.path_plot, time_plot)+'.png'
        plt.savefig(save_plot)
        return time_plot, save_plot


class Send_notification():
    """ Class to send a notification in case something is wrong"""
    def __init__(self):
 
        self.port = 465  # For SSL
        self.smtp_server = "smtp.gmail.com"
        self.sender_email = "isoclimlab@gmail.com"  # Enter your address
        self.receiver_email = ["pirmin.ebner@uib.no"]  # Enter receiver address
        self.password = "hpiv ysuf ogfq iras"

        
    def send_email(self, text, save_fig):
        """ Send an Email"""
        for receiver in self.receiver_email:
            message = MIMEMultipart()
            message['Subject'] = "FREEZER ALARM!"
            message['From'] = self.sender_email
            message['To'] = receiver
            # Add body to email
            message.attach(MIMEText(text,'plain'))

            filename = save_fig
            # Open file in binary mode
            with open(filename, 'rb') as attachment:
                # Add file as application/octet-stream
                # Email client can usually download this automatically as attachment
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())

            # Encode file in ASCII characters to send by email
            encoders.encode_base64(part)
            # Add header as key/value pair to attachment part
            part.add_header('Content-Disposition', f'attachment; filename= {filename}',)
            # Add attachment to message and convert message to string
            message.attach(part)
            text = message.as_string()
            # Log in to server using secure contex and send email
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:
                server.login(self.sender_email, self.password)
                server.sendmail(self.sender_email, receiver, text)

def main():
    # Class to get temperature sensor data
    get_temperature = Read_temperature()
    # Class to send notification
    send_not = Send_notification()

    while True:
        # Get current timestamp
        current_time = datetime.datetime.now()
        # Actual file to write per each day
        file_name = f"sensor_data_{current_time.strftime('%Y-%m-%d')}.csv"
        # Log sensor data
        get_temperature.log_sensor_data(file_name)
        # If
        ct, temp_c = get_temperature.read_temp()
        if temp_c >= -3:
            print('ALARM: Time: {}, Temperature (C): {}'.format(ct, temp_c))
            # Get temperature figure
            time_plot, save_plot = get_temperature.get_figure()
            # Send notification
            text = 'Critical temperature of the freezer {}, Time {}'.format(temp_c, ct)
            send_not.send_email(text, save_plot)
            time.sleep(0.2)

if __name__ == "__main__":
    main()

